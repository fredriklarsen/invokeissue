

# Issue with invoke 

Simple reproducible project for the issue where an extension function on operator invoke is not 
resolved in the IDE across modules.

The issue is that the IDE cannot resolve the correct import (and thus not suggest) for the extension
 function, if the operator invoke syntax is used. I.e. if `logger { }` is used and note `logger.invoke {}`  
  
See the class Consumer for details:

```kotlin
package com.subgarden.invokeissue

import com.subgarden.library.Logger
import com.subgarden.library.invoke // <-- has to be added manually when using the operator syntax

class Consumer(logger: Logger) {

    init {

        // The IDE is NOT able to resolve this and does not suggest the correct import.
        // However, it works when the import is added.
        logger {}

        // The IDE is able to resolve this and suggest the correct import.
        logger.invoke {}
    }

}
```
