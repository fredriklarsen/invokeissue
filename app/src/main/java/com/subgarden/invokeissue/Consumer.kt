package com.subgarden.invokeissue

import com.subgarden.library.Logger
import com.subgarden.library.invoke // <-- has to be added manually when using the operator syntax

class Consumer(logger: Logger) {

    init {

        // The IDE is NOT able to resolve this and dos not suggest the correct import
        logger {}

        // The IDE is able to resolve this and suggest the correct import
        logger.invoke {}
    }

}